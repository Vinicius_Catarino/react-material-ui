import React, { Component, Fragment } from "react";

import Header from '../../components/layouts/Header'
import Index from '../../components/layouts/Index'

export default class Main extends Component {
  render() {
    return <Fragment>
        <Header />
        <Index />
    </Fragment>
  }
}
