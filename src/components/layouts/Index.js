import React from 'react'

import {Grid, Paper} from '@material-ui/core'
import './styles.css'

const style = {
    Grid: {padding: 20},
    Paper: {padding: 10, marginTop: 10, marginBotton: 10},
    blue: { backgroundColor: "blue"}
}

export default props =>
    <Grid container>
        <Grid item xs style={style.Grid}>
            <h2>Receber</h2>
            <Paper style={{...style.Paper, ...style.blue}}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
        </Grid>
        <Grid item xs style={style.Grid}>
            <h2>Conferir</h2>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>

            
        </Grid>
        <Grid item xs style={style.Grid}>
            <h2>Passar</h2>        
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
            
            <Paper style={style.Paper}>
                <p>João Gabriel Medina Santos</p>
            </Paper>
        </Grid>
    </Grid>
