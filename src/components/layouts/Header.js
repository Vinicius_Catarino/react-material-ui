import React from 'react'

import {AppBar, Toolbar, IconButton} from '@material-ui/core';
import MenuIcon from '@material-ui/icons/Menu';

export default props =>
    <AppBar position="static">
        <Toolbar>
            <IconButton className="menuButton" color="inherit" aria-label="Menu">
                <MenuIcon />
            </IconButton>
        </Toolbar>
    </AppBar>